package ir.maktab43.service.dto;

public class AdminDTO extends UserDTO {

    private Boolean isSuperAdmin;

    public Boolean getSuperAdmin() {
        return isSuperAdmin;
    }

    public void setSuperAdmin(Boolean superAdmin) {
        isSuperAdmin = superAdmin;
    }
}
