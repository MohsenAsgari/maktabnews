package ir.maktab43.service.dto;

import ir.maktab43.base.BaseDTO;

public class TagDTO extends BaseDTO<Long> {

    private Long id;

    private String title;

    public TagDTO(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
