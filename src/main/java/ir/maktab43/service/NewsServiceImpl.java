package ir.maktab43.service;

import ir.maktab43.domains.Admin;
import ir.maktab43.domains.Category;
import ir.maktab43.domains.News;
import ir.maktab43.domains.User;
import ir.maktab43.domains.enums.NewsType;
import ir.maktab43.util.Database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class NewsServiceImpl {

    private TagServiceImpl tagService = new TagServiceImpl();

    private CategoryServiceImpl categoryService = new CategoryServiceImpl();


    public News saveOrUpdateArticleNews(News news) {
        User currentUser = Database.getCurrentUser();
        if (currentUser != null) {
            if (news.getId() == null) {
                return saveArticleNews(news);
            } else {
                return updateArticleNews(news);
            }
        } else {
            throw new RuntimeException("access denied");
        }
    }

    private News saveArticleNews(News news) {
//        news.setCreateDate(Date.from(Instant.now()));
        news.setCreateDate(new Date());
        news.setCreator(Database.getCurrentUser());
        news.setTags(tagService.saveOrUpdateTags(news.getTags()));
        checkCategory(news.getCategory());
        news.setType(NewsType.ARTICLE);
        news.setChosenForSlider(Boolean.FALSE);
        return Database.addNewNews(news);
    }

    private void checkCategory(Category category) {
        if (category != null) {
            categoryService.checkCategoryChildren(category);
//            categoryService.checkCategoryChildrenById(category.getId());
        } else {
            throw new RuntimeException("null category");
        }
    }

    private News updateArticleNews(News news) {
        if (Database.getNewsList().stream().anyMatch(
                selectedNews -> selectedNews.getId().equals(news.getId()))) {
            News myNews = Database.getNewsList().stream()
                    .filter(selectedNews -> selectedNews.getId().equals(news.getId())).findFirst().get();
            if (!(Database.getCurrentUser() instanceof Admin)) {
                if (!myNews.getCreator().getId().equals(Database.getCurrentUser().getId())) {
                    throw new RuntimeException("access denied");
                }
            }
            news.setTags(tagService.saveOrUpdateTags(news.getTags()));
            checkCategory(news.getCategory());
            return Database.replaceNews(news);
        } else {
            throw new RuntimeException("there is no news with id : " + news.getId());
        }
    }

    public void updateIsChosenForSlider(Long newsId) {
        if (newsId != null) {
            User currentUser = Database.getCurrentUser();
            if (currentUser instanceof Admin) {
                if (Database.getNewsList().stream().anyMatch(news -> news.getId().equals(newsId))) {
                    News selectedNews = Database.getNewsList()
                            .stream()
                            .filter(news -> news.getId().equals(newsId)).findFirst().get();
                    if (selectedNews.getChosenForSlider() != null) {
                        selectedNews.setChosenForSlider(!selectedNews.getChosenForSlider());
                    } else {
                        selectedNews.setChosenForSlider(true);
                    }
                } else {
                    throw new RuntimeException("there is no news with id : " + newsId);
                }
            } else {
                throw new RuntimeException("access denied");
            }
        } else {
            throw new RuntimeException("null newsId");
        }
    }

    public List<News> getSliderNews() {
        if (!Database.getNewsList().isEmpty()) {
            return Database.getNewsList().stream()
                    .filter(news ->
                            news.getChosenForSlider() != null && news.getChosenForSlider())
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

}
