package ir.maktab43.service;

import ir.maktab43.domains.Category;
import ir.maktab43.util.Database;

public class CategoryServiceImpl {

    public Boolean checkCategoryChildren(Category category) {
        if (category != null && category.getId() != null) {
            return checkCategory(category.getId());
        } else {
            throw new RuntimeException("null category");
        }
    }

    public Boolean checkCategoryChildrenById(Long categoryId) {
        if (categoryId != null) {
            return checkCategory(categoryId);
        } else {
            throw new RuntimeException("null category");
        }
    }

    private Boolean checkCategory(Long categoryId) {
        if (Database.getCategoryList().stream().anyMatch(cat -> cat.getId().equals(categoryId))) {
            Category selectedCategory = Database.getCategoryList().stream().filter(cat -> cat.getId().equals(categoryId))
                    .findFirst().get();
            if (selectedCategory.getChildrenCategories() == null || selectedCategory.getChildrenCategories().isEmpty()) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new RuntimeException("there is no category with id : " + categoryId);
        }
    }
}
