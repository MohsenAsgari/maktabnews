package ir.maktab43.service.impl;

import ir.maktab43.domains.User;
import ir.maktab43.repository.UserRepository;
import ir.maktab43.service.UserService;

public class UserServiceImpl extends BaseUserServiceImpl<User, UserRepository> implements UserService {

    public UserServiceImpl(UserRepository repository) {
        super(repository);
    }
}
