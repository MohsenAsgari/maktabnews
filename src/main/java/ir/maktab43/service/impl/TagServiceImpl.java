package ir.maktab43.service.impl;

import ir.maktab43.base.service.impl.BaseServiceImpl;
import ir.maktab43.domains.Tag;
import ir.maktab43.repository.TagRepository;
import ir.maktab43.service.TagService;

import java.util.List;

public class TagServiceImpl extends BaseServiceImpl<Tag, Long, TagRepository> implements TagService {

    public TagServiceImpl(TagRepository repository) {
        super(repository);
    }

    @Override
    public List<Tag> findAllByName(String name) {
//        logic
        return repository.findAllByName(name);
    }
}
