package ir.maktab43.service.impl;

import ir.maktab43.base.service.impl.BaseServiceImpl;
import ir.maktab43.domains.User;
import ir.maktab43.repository.BaseUserRepository;
import ir.maktab43.service.BaseUserService;

public class BaseUserServiceImpl<E extends User, Repo extends BaseUserRepository<E>> extends BaseServiceImpl<E, Long, Repo>
        implements BaseUserService<E> {

    public BaseUserServiceImpl(Repo repository) {
        super(repository);
    }
}
