package ir.maktab43.service.impl;

import ir.maktab43.domains.Admin;
import ir.maktab43.repository.AdminRepository;
import ir.maktab43.service.AdminService;

public class AdminServiceImpl extends BaseUserServiceImpl<Admin, AdminRepository> implements AdminService {

    public AdminServiceImpl(AdminRepository repository) {
        super(repository);
    }

    @Override
    public Admin save(Admin admin) {
        return super.save(admin);
    }
}
