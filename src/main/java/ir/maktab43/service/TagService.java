package ir.maktab43.service;

import ir.maktab43.base.service.BaseService;
import ir.maktab43.domains.Tag;

import java.util.List;

public interface TagService extends BaseService<Tag, Long> {

    List<Tag> findAllByName(String name);

}
