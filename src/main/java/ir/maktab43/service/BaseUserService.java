package ir.maktab43.service;

import ir.maktab43.base.service.BaseService;
import ir.maktab43.domains.User;

public interface BaseUserService<E extends User> extends BaseService<E, Long> {
}
