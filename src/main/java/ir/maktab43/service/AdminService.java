package ir.maktab43.service;

import ir.maktab43.domains.Admin;

public interface AdminService extends BaseUserService<Admin> {
}
