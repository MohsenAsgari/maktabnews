package ir.maktab43.service;

import ir.maktab43.domains.Tag;
import ir.maktab43.util.Database;

import java.util.HashSet;
import java.util.Set;

public class TagServiceImpl {

    public Set<Tag> saveOrUpdateTags(Set<Tag> tags) {
        Set<Tag> tagSet = new HashSet<>();
        if (tags != null && !tags.isEmpty()) {
            tagSet.forEach(tag -> {
                if (existsByTitle(tag.getTitle())) {
                    tagSet.add(
                            getByTitle(tag.getTitle())
                    );
                } else {
                    tagSet.add(
                            saveNewTag(tag)
                    );
                }
            });
        }
        return tagSet;
    }

    private Boolean existsByTitle(String title) {
        Set<Tag> tagSet = Database.getTagSet();
        if (!tagSet.isEmpty()) {
            return tagSet.stream().anyMatch(tag -> tag.getTitle().equals(title));
        } else {
            return false;
        }
    }

    private Tag saveNewTag(Tag tag) {
        return Database.addNewTag(tag);
    }

    private Tag getByTitle(String title) {
        return Database.getTagSet()
                .stream()
                .filter(tag -> tag.getTitle().equals(title))
                .findFirst().get();
    }

}
