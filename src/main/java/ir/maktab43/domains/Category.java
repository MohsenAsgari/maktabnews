package ir.maktab43.domains;

import ir.maktab43.base.BaseEntity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Category {

    private Long id;

    private String title;

    private String description;

    private Boolean isChosenForHeader;

    //    @ManyToMany
//    @OneToOne
//    @OneToMany
//    @ManyToOne *
    private Category parent;

    //    @OneToMany
    private Set<Category> childrenCategories = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getChosenForHeader() {
        return isChosenForHeader;
    }

    public void setChosenForHeader(Boolean chosenForHeader) {
        isChosenForHeader = chosenForHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public Set<Category> getChildrenCategories() {
        return childrenCategories;
    }

    public void setChildrenCategories(Set<Category> childrenCategories) {
        this.childrenCategories = childrenCategories;
    }
}

class BaseCategory<E extends BaseEntity<PK>, PK extends Serializable> extends BaseEntity<PK> {

    private E parent;

    //    @OneToMany
    private Set<E> childrenCategories = new HashSet<>();

    public E getParent() {
        return parent;
    }

    public void setParent(E parent) {
        this.parent = parent;
    }

    public Set<E> getChildrenCategories() {
        return childrenCategories;
    }

    public void setChildrenCategories(Set<E> childrenCategories) {
        this.childrenCategories = childrenCategories;
    }
}

class FaqCategory extends BaseCategory<FaqCategory, Long> {

    public void add() {
        FaqCategory faqCategory = new FaqCategory();
        Set<FaqCategory> childrenCategories = faqCategory.getChildrenCategories();
    }
}


class TicketCategory extends BaseCategory<TicketCategory, Long> {

}
