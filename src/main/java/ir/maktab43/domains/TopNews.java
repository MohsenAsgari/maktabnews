package ir.maktab43.domains;

import ir.maktab43.domains.enums.TopNewsType;

import java.util.HashSet;
import java.util.Set;

public class TopNews {

    private Long id;

    private Long topNewsOrder;

    private Tag tag;

    private Category category;

    private TopNewsType topNewsType;

    private Set<News> newsSet = new HashSet<>();

}
