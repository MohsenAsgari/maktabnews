package ir.maktab43.util;

public class Menu {

    private Menu() {
        throw new RuntimeException("can't create instance of this class");
    }

    public static void showStartMenu() {
        System.out.println("welcome!!!");
        System.out.println("1.Login");
        System.out.println("2.sign up");
        System.out.println("3.free look");
        System.out.println("4.exit");
        System.out.println("please select number:");
    }

    public static void showLoginMenu() {
        System.out.println("Enter Your Username:");
        System.out.println("Enter Your Password:");
    }

    public static void showAdminMenu() {
        System.out.println(" hello " + Database.getCurrentUser().getFirstName());
        System.out.println("1. my news");
        System.out.println("2. category management");
        System.out.println("3. all news");
        System.out.println("4. all published news");
        System.out.println("5. add news");
        System.out.println("6. logout");
        System.out.println("please select number:");
    }

    public static void showSingleNewsMenu() {
        System.out.println("1. edit");
        System.out.println("2. remove");
        System.out.println("3. publish");
        System.out.println("4. un publish");
        System.out.println("5. back");
        System.out.println("6. exit");
    }
}
