package ir.maktab43.util;

import ir.maktab43.domains.Admin;
import ir.maktab43.domains.News;
import ir.maktab43.domains.User;

import java.util.List;
import java.util.Scanner;

public class Operations {

    private static final Scanner stringInput = new Scanner(System.in);
    private static final Scanner numberInput = new Scanner(System.in);

    public static void doOpearionAfterStartMenu() {
        try {
            int i = numberInput.nextInt();
            switch (i) {
                case 1: {
                    doOperationForLogin();
                    break;
                }
                case 2: {
                    doOperationForSignUp();
                    break;
                }
                case 3: {
                    doOperationFreeLook();
                    break;
                }
                case 4: {
                    System.exit(100);
                }
                default:
                    throw new RuntimeException("wrong number");
            }
        } catch (Exception e) {
            System.out.println("please enter a number:");
            doOpearionAfterStartMenu();
        }
    }

    private static void doOperationFreeLook() {

    }

    private static void doOperationForSignUp() {

    }

    private static void doOperationForLogin() {
        System.out.println("please enter your username:");
        String username = stringInput.next();
        System.out.println("please enter your password:");
        String password = stringInput.next();

        if (Database.getUserList().stream().anyMatch(
                user -> user.getUsername().equals(username)
        )) {
            User currentUser = Database.getUserList().stream().filter(user ->
                    user.getUsername().equals(username)).findFirst().get();
            if (currentUser.getPassword().equals(password)) {
                Database.setCurrentUser(currentUser);
                if (currentUser instanceof Admin) {
                    doOperationForAdminAfterLogin();
                } else {
//                    TODO complete me
                }
            } else {
                System.out.println("wrong password!!!");
                doOpearionAfterStartMenu();
            }
        } else {
            System.out.println("wrong username!!!");
            doOpearionAfterStartMenu();
        }

    }

    private static void doOperationForAdminAfterLogin() {
        Menu.showAdminMenu();
        try {
            int i = numberInput.nextInt();
            switch (i) {
                case 1: {
                    doOperationForMyNews();
                    break;
                }
                case 2: {
                    doOperationForCategoryManagement();
                    break;
                }
                case 3: {
                    doOperationForAllNews();
                    break;
                }
                case 4: {
                    doOperationForAllPublishedNews();
                    break;
                }
                case 5: {
                    doOperationForAddNews();
                    break;

                }
                case 6: {
                    doOperationForLogout();
                    break;

                }
                default:
                    throw new RuntimeException("wrong number");
            }
        } catch (Exception e) {
            System.out.println("please enter a number:");
            doOperationForAdminAfterLogin();
        }
    }

    private static void doOperationForAddNews() {

    }

    private static void doOperationForAllPublishedNews() {

    }

    private static void doOperationForLogout() {

    }

    private static void doOperationForAllNews() {

    }

    private static void doOperationForCategoryManagement() {

    }

    private static void doOperationForMyNews() {
        List<News> currentUserNewsList = Database.getCurrentUserNewsList();
        if (currentUserNewsList.isEmpty()) {
            System.out.println(" you don't have any news yet !!!");
            doOperationForAdminAfterLogin();
        } else {
            currentUserNewsList.forEach(news ->
                    System.out.println("news id: " + news.getId() + ", news title:" + news.getTitle()));
            selectNewsAnotherVersion(currentUserNewsList);
        }
    }

    private static void selectNewsAnotherVersion(List<News> currentUserNewsList) {
        System.out.println(" select one of your news id or enter 0 for prev menu:");
        try {
            long selectedId = numberInput.nextLong();
            if (currentUserNewsList.stream().anyMatch(
                    news -> news.getId().equals(selectedId))) {
                doOperationForShowingSingleNews(
                        currentUserNewsList.stream().filter(
                                news -> news.getId().equals(selectedId))
                                .findFirst().get()
                );
            } else if (selectedId == 0) {
                doOperationForAdminAfterLogin();
            } else {
                System.out.println("no such id, try again");
                doOperationForMyNews();
            }
        } catch (Exception e) {
            doOperationForMyNews();
        }

    }

    private static void selectNews(List<News> currentUserNewsList) {
        System.out.println(" select one of your news id or enter any word for prev menu:");
        try {
            long selectedId = numberInput.nextLong();
            if (currentUserNewsList.stream().anyMatch(
                    news -> news.getId().equals(selectedId))) {

                doOperationForShowingSingleNews(
                        currentUserNewsList.stream().filter(
                                news -> news.getId().equals(selectedId))
                                .findFirst().get()
                );

            } else {
                System.out.println("no such id, try again");
                doOperationForMyNews();
            }
        } catch (Exception e) {
            doOperationForAdminAfterLogin();
        }

    }

    private static void doOperationForShowingSingleNews(News news) {
        System.out.println("news title : " + news.getTitle());
        System.out.println("news content : " + news.getContent());
        System.out.println("news category : " + news.getCategory().getTitle());
        System.out.println("news createDate : " + news.getCreateDate());
        Menu.showSingleNewsMenu();
        try {
            int selectedNumber = numberInput.nextInt();
            switch (selectedNumber) {
                case 1: {
                    doOperationForEditNews();
                    break;
                }
                case 2: {
                    doOperationForRemoveNews();
                    break;
                }
                case 3: {
                    doOperationForPublishNews();
                    break;
                }
                case 4: {
                    doOperationForUnPublishNews();
                    break;
                }
                case 5: {
                    doOperationForMyNews();
                    break;
                }
                case 6: {
                    System.exit(100);
                }
                default: {
                    System.out.println("please select a correct number");
                    doOperationForShowingSingleNews(news);
                    break;
                }
            }
        } catch (Exception e) {
            doOperationForMyNews();
        }
    }

    private static void doOperationForPublishNews() {

    }

    private static void doOperationForUnPublishNews() {

    }

    private static void doOperationForRemoveNews() {

    }

    private static void doOperationForEditNews() {

    }
}
