package ir.maktab43.util;

import com.github.javafaker.Faker;
import ir.maktab43.domains.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Database {

    private static Long databaseSequence = 1L;

    private static List<User> userList = new ArrayList<>();

    private static User currentUser;

    private static List<News> newsList = new ArrayList<>();

    private static List<Category> categoryList = new ArrayList<>();

    private static Set<Tag> tagSet = new HashSet<>();

    private static Faker faker = new Faker();

    public static List<News> getNewsList() {
        return newsList;
    }

    public static List<Category> getCategoryList() {
        return categoryList;
    }

    private static Long getAndUpdateDatabaseSequence() {
        return databaseSequence++;
    }

    public static void initDataBase() {
        initUserList();
        initNewsList();
        initCategoryList();
    }

    private static void initCategoryList() {
        IntStream.range(0, 5)
                .forEach(i -> categoryList.add(createRandomCategory()));
    }

    private static Category createRandomCategory() {
        Category category = new Category();
        category.setId(getAndUpdateDatabaseSequence());
        category.setTitle(faker.name().title());
        IntStream.range(0, 2)
                .forEach(i -> {
                    Category child = new Category();
                    child.setId(getAndUpdateDatabaseSequence());
                    child.setTitle(faker.name().title());
                    child.setParent(category);
                    category.getChildrenCategories().add(child);
                });
        return category;
    }

    private static void initNewsList() {

    }

    private static void initUserList() {
        userList.add(
                getSuperAdmin()
        );
    }

    private static Admin getSuperAdmin() {
        Admin admin = new Admin();
        admin.setId(getAndUpdateDatabaseSequence());
        admin.setUsername("mr-admin");
        admin.setFirstName(faker.name().firstName());
        admin.setLastName(faker.name().lastName());
        admin.setPassword("123456");
        admin.setSuperAdmin(Boolean.TRUE);
        return admin;
    }

    public static List<User> getUserList() {
        return userList;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User user) {
        currentUser = user;
    }

    public static List<News> getCurrentUserNewsList() {
        if (newsList.stream().anyMatch(news ->
                news.getCreator().getId().equals(currentUser.getId()))) {
            return newsList.stream().filter(news ->
                    news.getCreator().getId().equals(currentUser.getId()))
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public static Set<Tag> getTagSet() {
        return tagSet;
    }

    public static Tag addNewTag(Tag tag) {
        tag.setId(getAndUpdateDatabaseSequence());
        tagSet.add(tag);
        return tag;
    }

    public static News addNewNews(News news) {
        news.setId(getAndUpdateDatabaseSequence());
        newsList.add(news);
        return news;
    }

    public static News replaceNews(News news) {
        Database.getNewsList().removeIf(
                selectedNews -> selectedNews.getId().equals(news.getId()));
        newsList.add(news);
        return news;
    }
}
