package ir.maktab43;

import ir.maktab43.util.Database;
import ir.maktab43.util.Menu;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
//        System.out.println( "Hello World!" );
        Database.initDataBase();
        Menu.showStartMenu();

    }

}
