package ir.maktab43.base.repository;

import ir.maktab43.base.BaseEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface BaseRepository<E extends BaseEntity<PK>, PK extends Serializable> {

    E save(E e);

    E update(E e);

    List<E> saveAll(List<E> all);

    List<E> findAll();

    Optional<E> findById(PK id);

    void delete(E e);

    void deleteById(PK id);

}
