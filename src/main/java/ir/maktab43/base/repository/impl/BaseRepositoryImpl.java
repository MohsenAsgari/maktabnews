package ir.maktab43.base.repository.impl;

import ir.maktab43.base.BaseEntity;
import ir.maktab43.base.repository.BaseRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public class BaseRepositoryImpl<E extends BaseEntity<PK>, PK extends Serializable>
        implements BaseRepository<E, PK> {

    protected final String connection = "connection";

    @Override
    public E save(E e) {
        System.out.println(connection + "saved!!!");
        return null;
    }

    @Override
    public E update(E e) {
        System.out.println(connection + "update!!!");
        return null;
    }

    @Override
    public List<E> saveAll(List<E> all) {
        System.out.println(connection + "saveAll!!!");
        return null;
    }

    @Override
    public List<E> findAll() {
        System.out.println(connection + "findAll!!!");
        return null;
    }

    @Override
    public Optional<E> findById(PK id) {
        System.out.println(connection + "findById!!!");
        return Optional.empty();
    }

    @Override
    public void delete(E e) {
        System.out.println(connection + "delete!!!");
    }

    @Override
    public void deleteById(PK id) {
        System.out.println(connection + "deleteById!!!");
    }
}
