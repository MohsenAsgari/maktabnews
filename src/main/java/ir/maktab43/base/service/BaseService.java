package ir.maktab43.base.service;

import ir.maktab43.base.BaseEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface BaseService<E extends BaseEntity<ID>, ID extends Serializable> {

    E save(E e);

    E update(E e);

    List<E> saveAll(List<E> all);

    List<E> findAll();

    Optional<E> findById(ID id);

    void delete(E e);

    void deleteById(ID id);
}
