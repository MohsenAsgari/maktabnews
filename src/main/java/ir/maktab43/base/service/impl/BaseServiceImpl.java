package ir.maktab43.base.service.impl;

import ir.maktab43.base.BaseEntity;
import ir.maktab43.base.repository.BaseRepository;
import ir.maktab43.base.service.BaseService;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public class BaseServiceImpl<E extends BaseEntity<ID>, ID extends Serializable,
        Repository extends BaseRepository<E, ID>> implements BaseService<E, ID> {

    protected final Repository repository;

    public BaseServiceImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public E save(E e) {
        return repository.save(e);
    }

    @Override
    public E update(E e) {
        return repository.update(e);
    }

    @Override
    public List<E> saveAll(List<E> all) {
        return repository.saveAll(all);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<E> findById(ID id) {
        return repository.findById(id);
    }

    @Override
    public void delete(E e) {
        repository.delete(e);
    }

    @Override
    public void deleteById(ID id) {
        repository.deleteById(id);
    }
}
