//package ir.maktab43.base.resource;
//
//import ir.maktab43.base.BaseDTO;
//import ir.maktab43.base.BaseEntity;
//import ir.maktab43.base.service.BaseService;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//public class BaseResource<E extends BaseEntity<ID>, D extends BaseDTO<ID>,
//        ID extends Serializable, Service extends BaseService<E, ID>> {
//
//    protected final Service service;
//
//
////    protecte Mapper mapper;
//
//    public BaseResource(Service service) {
//        this.service = service;
//    }
//
//
//    public D save(D d) {
//        E save = service.save(convertToEntity(d));
//        return convertToDTO(save);
//    }
//
//    public D update(D d) {
//        E update = service.update(convertToEntity(d));
//        return convertToDTO(update);
//    }
//
//    public List<D> saveAll(List<D> all) {
//        List<E> saveAll = service.saveAll(convertToListEntity(all));
//        return convertToListDTO(saveAll);
//    }
//
//    public List<D> findAll() {
//        return
//                convertToListDTO(
//                        service.findAll()
//                );
//    }
//
//    public D findById(ID id) {
//        Optional<E> byId = service.findById(id);
//        if (byId.isPresent()) {
//            return convertToDTO(byId.get());
//        } else {
//            throw new RuntimeException("no entity");
//        }
//    }
//
//    public Boolean delete(E e) {
//        service.delete(e);
//        return true;
//    }
//
//    public Boolean deleteById(ID id) {
//        service.deleteById(id);
//        return true;
//    }
//
//    public E convertToEntity(D d) {
////        mapper.map(d, E.class);
//    }
//
//    public D convertToDTO(E e) {
//
//    }
//
//    public List<D> convertToListDTO(List<E> eList) {
//        List<D> dList = new ArrayList<>();
//        eList.forEach(e ->
//                dList.add(convertToDTO(e))
//        );
//        return dList;
//    }
//
//    public List<E> convertToListEntity(List<D> dList) {
//        List<E> eList = new ArrayList<>();
//        dList.forEach(d ->
//                eList.add(convertToEntity(d))
//        );
//        return eList;
//    }
//
//}
