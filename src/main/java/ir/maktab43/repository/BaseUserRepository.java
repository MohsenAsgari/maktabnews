package ir.maktab43.repository;

import ir.maktab43.base.repository.BaseRepository;
import ir.maktab43.domains.User;

public interface BaseUserRepository<E extends User> extends BaseRepository<E, Long> {
}
