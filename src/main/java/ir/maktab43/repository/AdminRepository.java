package ir.maktab43.repository;

import ir.maktab43.domains.Admin;

public interface AdminRepository extends BaseUserRepository<Admin> {
}
