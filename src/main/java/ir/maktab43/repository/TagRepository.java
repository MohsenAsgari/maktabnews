package ir.maktab43.repository;

import ir.maktab43.base.repository.BaseRepository;
import ir.maktab43.domains.Tag;

import java.util.List;

public interface TagRepository extends BaseRepository<Tag, Long> {

    List<Tag> findAllByName(String name);

}
