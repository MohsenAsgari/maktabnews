package ir.maktab43.repository.impl;

import ir.maktab43.domains.Admin;
import ir.maktab43.repository.AdminRepository;

public class AdminRepositoryImpl extends BaseUserRepositoryImpl<Admin> implements AdminRepository {
}
