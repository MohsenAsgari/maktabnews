package ir.maktab43.repository.impl;

import ir.maktab43.base.repository.impl.BaseRepositoryImpl;
import ir.maktab43.domains.User;
import ir.maktab43.repository.BaseUserRepository;

public class BaseUserRepositoryImpl<E extends User> extends BaseRepositoryImpl<E, Long> implements BaseUserRepository<E> {

}
